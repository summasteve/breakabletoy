package com.summatech.tburley.mytictactoe;

/**
 * Created by Toni on 9/16/2015.
 */
public class Board {
    char cell1 = '1';
    char cell2 = '2';
    char cell3 = '3';
    char cell4 = '4';
    char cell5 = '5';
    char cell6 = '6';
    char cell7 = '7';
    char cell8 = '8';
    char cell9 = '9';



    public void showBoard() {
        String row1 = cell1 + " | " + cell2 + " | " + cell3;
        String row2 = cell4 + " | " + cell5 + " | " + cell6;
        String row3 = cell7 + " | " + cell8 + " | " + cell9;
        System.out.println(row1 + "\n" + row2 + "\n" + row3);
    }


    public void addMove(char playerMove, int player) {
        char symbol = player == 1 ? 'X' : 'O';

        switch (playerMove) {
            case '1':
                cell1 = symbol;
                break;

            case '2':
                cell2 = symbol;
                break;

            case '3':
                cell3 = symbol;
                break;

            case '4':
                cell4 = symbol;
                break;

            case '5':
                cell5 = symbol;
                break;

            case '6':
                cell6 = symbol;
                break;
            case '7':
                cell7 = symbol;
                break;

            case '8':
                cell8 = symbol;
                break;

            case '9':
                cell9 = symbol;
                break;
        }
        showBoard();
    }
}